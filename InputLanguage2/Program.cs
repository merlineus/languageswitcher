﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace InputLanguage2
{
    static class Program
    {
        const int HC_ACTION = 0;
        const int WH_KEYBOARD_LL = 13;
        const int WM_KEYDOWN = 0x0100;
        const int WM_KEYUP = 0x0101;
        static IntPtr HookHandle = IntPtr.Zero;
        static Form1 Form1;
        static Bar Bar;

        static Boolean kWin, kSpace;

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr SetWindowsHookEx(int idHook, KbHook lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        private static extern bool PostMessage(IntPtr hhwnd, uint msg, IntPtr wparam, IntPtr lparam);

        [DllImport("user32.dll")]
        static extern IntPtr LoadKeyboardLayout(string pwszKLID, uint Flags);

        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);            
            kWin = false;
            kSpace = false;
            try
            {
                using (var proc = Process.GetCurrentProcess())
                using (var curModule = proc.MainModule)
                {
                    var moduleHandle = GetModuleHandle(curModule.ModuleName);
                    HookHandle = SetWindowsHookEx(WH_KEYBOARD_LL, IgnoreWin_Space, moduleHandle, 0);
                }
                Form1 = new Form1();
                Bar = new Bar();
                Application.Run(Form1);
            }
            finally
            {
                UnhookWindowsHookEx(HookHandle);
            }
        }

        [DllImport("user32.dll")]
        static extern short GetAsyncKeyState(Keys vKey);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode, IntPtr wParam, IntPtr lParam);

        static IntPtr IgnoreWin_Space(int nCode, IntPtr wParam, IntPtr lParam)
        {
            Boolean spacePressed = false;
            var keyInfo = (KbHookParam)Marshal.PtrToStructure(lParam, typeof(KbHookParam));

            if (nCode == HC_ACTION)
            {                
                if ((int)wParam == WM_KEYDOWN)
                {
                    if (keyInfo.VkCode == (int)Keys.Space)
                    {
                        spacePressed = true;                        
                        kSpace = true;
                    }
                    else
                        kSpace = false;

                    // нажат одновременно левый виндовс
                    if (GetAsyncKeyState(Keys.LWin) < 0)
                        kWin = true;
                    else
                    {
                        kWin = false;
                    }

                    if (kWin && kSpace)
                    {                        
                        if (spacePressed)
                        {
                            Bar.SetLanguage("");
                            Bar.Show(); // сбивает фокус, пофиксим в конструкторе
                            return (IntPtr)1; //just ignore the key press
                        }
                    }
                }
            }
            if ((int)wParam == WM_KEYUP)
            {
                if (keyInfo.VkCode == (int)Keys.LWin)
                {                    
                    kWin = false;
                    Bar.DoHide();
                    string HEX = Bar.getHex();
                    uint WM_INPUTLANGCHANGEREQUEST = 0x0050;
                    uint KLF_ACTIVATE = 1;
                    PostMessage(GetForegroundWindow(), WM_INPUTLANGCHANGEREQUEST, IntPtr.Zero, LoadKeyboardLayout(HEX, KLF_ACTIVATE));
                }                
            }

            return CallNextHookEx(HookHandle, nCode, wParam, lParam);
        }       

        delegate IntPtr KbHook(int nCode, IntPtr wParam, [In] IntPtr lParam);

        [StructLayout(LayoutKind.Sequential)]
        struct KbHookParam
        {
            public readonly int VkCode;
            public readonly int ScanCode;
            public readonly int Flags;
            public readonly int Time;
            public readonly IntPtr Extra;
        }
    }
}
